#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "RS-232/rs232.h"

#if defined (WIN32)
    #include <winsock2.h>

    typedef int socklen_t;
#elif defined (linux)
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>

    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
    #define closesocket(s) close(s)

    typedef int SOCKET;
    typedef struct sockaddr_in SOCKADDR_IN;
    typedef struct sockaddr SOCKADDR;
#endif // defined

#define BUF_SIZE 128
#define PORT 23

int main()
{
    /* SOCKET PART */
    #if defined (WIN32)
        WSADATA WSAData;
        int erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
    #else
        int erreur = 0;
    #endif // defined

    SOCKET sock;
    SOCKADDR_IN sin;
    int sock_err;
    char ip[16];

    if(!erreur)
    {
        sock = socket(AF_INET, SOCK_STREAM, 0);
	
	printf("What is the server ip address ?\n");
	scanf("%s", &ip);
	sin.sin_addr.s_addr = inet_addr(ip);
        sin.sin_family = AF_INET;
        sin.sin_port = htons(PORT);

        if(connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != SOCKET_ERROR)
        {
            printf("Connection to %s on port %d\n", inet_ntoa(sin.sin_addr), htons(sin.sin_port));

            /* ARDUINO SERIAL PART */
            int cport_nr = 24; /* dev/ttyACM0 */
            int bdrate = 57600;

            char mode[] = {'8', 'N', '1', 0}; /* 8 data bits, no parity, 1 stop bit */
            unsigned char strSendData[BUF_SIZE]; /* recv data buffer */
	    char strRecvOk[2];
            /*char strSendOk[2]; send signal to Arduino to allow data reception */
            
            //strcpy(strSendOk, "S");

            if(RS232_OpenComport(cport_nr, bdrate, mode, 1))
            {
                printf("Can not open comport\n");
                return(0);
            }

            sleep(2); /* wait for stable condition */

            while(1)
            {            	
                int m = RS232_SendByte(cport_nr, 'S'); /* send data on serial */
		printf("SendByte() return value : %i\n", m);

                usleep(1000000);
                int n = RS232_PollComport(cport_nr, strSendData, (int)BUF_SIZE); /* write data in strRecv & return nb bytes */
		printf("%s (%i bytes)\n", (char *)strSendData, n);

                if(n > 0)
                {
                    strSendData[n] = 0; /* always put null value at the end of string */
              	    sock_err = send(sock, &strSendData, sizeof(strSendData), 0);
                    if(sock_err != SOCKET_ERROR)
                    {
                        printf("Data sent !\n");
                    }
                    else
                    {
                        printf("Sending data error\n");
                    }

		    if(recv(sock, &strRecvOk, sizeof(strRecvOk), 0) != SOCKET_ERROR)
		    {
			    printf("Receiving server is ok\n");
		    }
		    else
		    {
			    printf("Server is not ok\n");
		    }
                }
		else
		{
			printf("No data from serial\n");			
      		}
	    }
        }
        else
        {
            printf("connection impossible\n");
        }

        closesocket(sock);

        #if defined (WIN32)
            WSACleanup();
        #endif // defined
    }

    getchar();

    return EXIT_SUCCESS;
}
