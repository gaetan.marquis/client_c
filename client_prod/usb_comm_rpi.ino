/* JOYSTICK */
const int VRX = 2; // analog pin 2
const int VRY = 3; // analog pin 3

int calX, calY;
int rawX, rawY;

/* LIGHT SENSOR */
const int lightSensorPin = 0; // analog pin 0
int light = 0;

String strSerial;

void setup() {
  Serial.begin(57600); // opens serial port, sets data rate to 57600 baud

  calX = analogRead(VRX);
  calY = analogRead(VRY);
}

void loop() {
  delay(1000);

  rawX = analogRead(VRX) - calX;
  rawY = analogRead(VRY) - calY;
  light = analogRead(lightSensorPin);

  strSerial = String(rawX) + "/" + String(rawY) + "/" + String(light);
  char cTabSerial[strSerial.length()+1];
  strSerial.toCharArray(cTabSerial, strSerial.length()+1);
  if(Serial.read() == 'S'){ // S for Send
    Serial.write(cTabSerial);
  }


  /*
  while (Serial.available() > 0) { // if any data available
    char incomingByte = Serial.read(); // read byte
    Serial.write(incomingByte); // send it back
  }
  */
}
